#!/bin/bash
if [ -z "$1" ]; then
	find /home/ifpb -maxdepth 1 -type f | wc -l
	find /home/ifpb -maxdepth 1 -type d | wc -l
else	
	find $1 -maxdepth 1 -type f | wc -l
	find $1 -maxdepth 1 -type d | wc -l
fi
